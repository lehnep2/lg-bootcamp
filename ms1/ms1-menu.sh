#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
echo -e "(1) Shutdown\n(2) Restart\n(3) Turn Screen\n(4) Keyboard Language\n"
echo "Enter the operation: "
read operation

case $operation in
1) 
    echo "Shutting down..."
        shutdown ;;
2)
    echo "Rebooting..."
        shutdown -r ;;
3)
    echo "Which direction?"
    read direcao
    if [ $direcao == "left" ]; then
        xrandr -o left
    elif [$direcao == "right" ]; then
        xrandr -o right 
    else
        echo "You didn't answer correctly!"
    fi ;;
4)
    echo "What is the language code that you want?"
    read lang
    setxkbmap $lang
    echo "The keyboard language changed for" $lang ;;
*)
    echo "Choose a valid option" ;;
esac
    #END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
